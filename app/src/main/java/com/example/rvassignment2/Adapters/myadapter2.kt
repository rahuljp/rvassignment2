package com.example.rvassignment2.Adapters

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.transition.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rvassignment2.Models.Items
import com.example.rvassignment2.R
import kotlinx.android.synthetic.main.listitem.view.*

class myadapter2(list : ArrayList<Items>) : RecyclerView.Adapter<myadapter2.viewholder>() {
    var list=list
    lateinit var context : Context
    class viewholder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        var itemView2=itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewholder {
       context=parent.context
        var view = LayoutInflater.from(context).inflate(R.layout.listitem,parent,false)
        view.checkBox.buttonDrawable=ColorDrawable(Color.TRANSPARENT)
        return viewholder(view)

    }

    override fun onBindViewHolder(holder: viewholder, position: Int) {
        var cp=holder.adapterPosition
        holder.itemView2.checkBox.text = list[cp].name
    }

    override fun getItemCount(): Int {
        return list.size
    }
}