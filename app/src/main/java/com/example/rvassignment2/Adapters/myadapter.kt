package com.example.rvassignment2.Adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import com.example.rvassignment2.MainActivity2
import com.example.rvassignment2.Models.Items
import com.example.rvassignment2.R
import java.io.Serializable

class myadapter(list: ArrayList<Items>) : RecyclerView.Adapter<myadapter.viewholder>() {
    var list=list
    lateinit var context:Context
    class viewholder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        var checkBox =itemView.findViewById<CheckBox>(R.id.checkBox)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewholder {
        context=parent.context
        var view=LayoutInflater.from(context).inflate(R.layout.listitem,parent,false)
        return viewholder(view)
    }

    override fun onBindViewHolder(holder: viewholder, position: Int) {
        holder.checkBox.setText(list.get(holder.adapterPosition).name)
        holder.checkBox.isChecked=list.get(holder.adapterPosition).checked
        holder.checkBox.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                list.get(holder.adapterPosition).checked = p0!!.isChecked
            }

        })
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun clearAll() {
        list.forEach {
            it.checked = false
        }

        notifyDataSetChanged()
    }

    fun selectAll() {
        list.forEach {
            it.checked = true
        }

        notifyDataSetChanged()
    }

    fun removeselected() {


        var y=list.size-1
        var i=0
        while (i<=y){
            if (list.get(i).checked){
                if (i==y){
                    list.removeAt(i)
                    notifyItemRemoved(i)
                    notifyItemRangeChanged(i,list.size)
                    return
                }
                list.removeAt(i)
                notifyItemRemoved(i)
                notifyItemRangeChanged(i,list.size)
                y--
            }
            else{
                i++
            }
        }








    }

    fun next() {
        var l1= arrayListOf<Items>()
        for (i in list){
            if (i.checked) l1.add(i)
        }
        var intent=Intent(context,MainActivity2::class.java)
        intent.putExtra("data",l1)
        context.startActivity(intent)
    }
}