package com.example.rvassignment2.Models

import java.io.Serializable

data class Items(val name : String, var checked : Boolean) : Serializable
