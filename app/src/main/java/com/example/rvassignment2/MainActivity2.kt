package com.example.rvassignment2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rvassignment2.Adapters.myadapter2
import com.example.rvassignment2.Models.Items
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        var data=intent.getSerializableExtra("data") as ArrayList<Items>
        Log.d("TAG",data.toString())
        recyclerView2.layoutManager=LinearLayoutManager(this)
        recyclerView2.adapter=myadapter2(data)
    }
}