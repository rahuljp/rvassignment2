package com.example.rvassignment2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rvassignment2.Adapters.myadapter
import com.example.rvassignment2.Models.Items
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter : myadapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var list= arrayListOf<Items>()
        list.add(Items("Item1",false))
        list.add(Items("Item2",false))
        list.add(Items("Item3",false))
        list.add(Items("Item4",false))
        list.add(Items("Item5",false))
        list.add(Items("Item6",false))
        list.add(Items("Item7",false))
        list.add(Items("Item8",false))
        list.add(Items("Item9",false))
        list.add(Items("Item10",false))
        list.add(Items("Item11",false))
        list.add(Items("Item12",false))
        list.add(Items("Item13",false))
        list.add(Items("Item14",false))
        list.add(Items("Item15",false))
        list.add(Items("Item16",false))




        recyclerView.layoutManager=LinearLayoutManager(this)
        adapter=myadapter(list)
        recyclerView.adapter=adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        MenuInflater(this).inflate(R.menu.menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.clear -> adapter.clearAll()
            R.id.selctall -> adapter.selectAll()
            R.id.remove -> adapter.removeselected()
            R.id.next -> adapter.next()

        }
        return true
    }
}